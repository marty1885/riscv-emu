#include <iostream>
#include <array>
#include <string>
#include <vector>
#include <utility>

#include <stdint.h>
#include <assert.h>

constexpr size_t memory_size = (1 << 20); //1MB
uint8_t* main_memory = nullptr;
size_t program_counter = 0;

class RV32Register
{
public:
	RV32Register()
	{
		for(auto& reg : registers_)
			reg = 0;
	}

	void set(uint32_t val, size_t idx)
	{
		if(idx != 0)
			registers_[idx] = val;
		//Write to reg 0 does nothing
	}
	
	uint32_t get(size_t idx) const
	{
		if(idx == 0)
			return 0;
		return registers_[idx];
	}
	
protected:
	std::array<uint32_t, 32> registers_;
};

class RV32Decoder
{
public:
	using InstructionFunc = void(*)(uint32_t, RV32Register&);
	
	void addInstruction(uint8_t op, InstructionFunc func)
	{
		operantions_.push_back(std::make_pair(op, func));
	}
	
	void execue(uint32_t machine_code, RV32Registers& registers)
	{
		uint8_t opcode = machine_code & 0x;
		//auto it = std::find_if(operantions_.begin(). operantions_.end(),
		//	[](const auto& operation){return operation.first ==})
	}
protected:
	std::vector<std::pair<uint8_t, InstructionFunc>> operantions_;
};

std::ostream& operator<< (std::ostream& os, const RV32Register& registers)
{
	for(size_t i=0;i<32;i++)
		os << "Reg " + std::to_string(i) + ": " + std::to_string(registers.get(i)) + "\n";
	return os;
}

int main()
{
	//Init main memory
	main_memory = new uint8_t[memory_size];
	
	RV32Register registers;
	
	while(true) {
		//Emulate the RISC-V ISA
		//std::cout << registers << std::endl;
		//break;
		
		
	}
	
	delete [] main_memory;
}
